# SPDX-FileCopyrightText: 2024 Galagic Limited, et al. <https://galagic.com>
#
# SPDX-License-Identifier: GPL-3.0-or-later
#
# Source of the website [innercitylearners.com](https://innercitylearners.com)
#
# For full copyright information see the AUTHORS file at the top-level
# directory of this distribution or at
# [AUTHORS](https://gitlab.com/thegalagic/innercitylearners_com/AUTHORS.md)
#
# This program is free software: you can redistribute it and/or modify it under
# the terms of the GNU General Public License as published by the Free Software
# Foundation, either version 3 of the License, or (at your option) any later
# version.
#
# This program is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
# FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
# details.
#
# You should have received a copy of the GNU General Public License along with
# this program. If not, see <https://www.gnu.org/licenses/>.

{ pkgs, lib, config, inputs, ... }:

{
  env.BUILDDIR = lib.mkDefault "build";
  # [Allow customizing the artifacts path](https://gitlab.com/groups/gitlab-org/-/epics/10126)
  # We are forced to use this location
  env.PUBLICDIR = lib.mkDefault "public";

  languages.go.enable = true;

  packages = [ pkgs.git
               pkgs.ninja
               pkgs.hugo
               pkgs.gitleaks
               pkgs.lychee
               pkgs.lsof    # for lsof use in lychee.sh
               pkgs.libxslt # for xsltproc use in lychee.sh
               pkgs.copier
               pkgs.reuse
               pkgs.brotli  # To compress assets for GitLab pages
               pkgs.ripgrep # For pre-commit hook below
               ];

  enterShell = ''
    # Feed environment to ninja
    echo "builddir="$BUILDDIR"" > .ninja.env
    echo "publicdir="$PUBLICDIR"" >> .ninja.env
  '';

  enterTest = ''
    echo "Running tests"
  '';

  processes = {
    hugo-dev.exec = "hugo server --buildDrafts --destination \"$BUILDDIR/hugo_server\" --bind 127.0.0.1 --disableFastRender --source hugo --cleanDestinationDir";
    stay.exec = "sleep 99d";
  };

  pre-commit.hooks.gitleaks = {
      # https://github.com/gitleaks/gitleaks/blob/master/.pre-commit-hooks.yaml
      enable = true;
      name = "Gitleaks";
      entry = "gitleaks protect --verbose --redact --staged";
      language = "golang";
      pass_filenames = false;
    };
  pre-commit.hooks.lychee = {
      # Until lychee supports recursive scan we have to do it ourselves
      enable = true;
      name = "Lychee";
      entry = "./lychee.sh";
      pass_filenames = false;
  };
  pre-commit.hooks.reuse = {
      enable = true;
      name = "REUSE";
      entry = "reuse lint";
      pass_filenames = false;
  };
  pre-commit.hooks.no-localhost-links = {
    enable = true;
    name = "No localhost links";
    entry = "bash -c '! rg -i -g \"hugo/content/**\" localhost || exit 1'";
    pass_filenames = false;
    };
}
