#!/usr/bin/env bash

# SPDX-FileCopyrightText: 2024 Galagic Limited, et al. <https://galagic.com>
#
# SPDX-License-Identifier: GPL-3.0-or-later
#
# Source of the website [innercitylearners.com](https://innercitylearners.com)
#
# For full copyright information see the AUTHORS file at the top-level
# directory of this distribution or at
# [AUTHORS](https://gitlab.com/thegalagic/innercitylearners_com/AUTHORS.md)
#
# This program is free software: you can redistribute it and/or modify it under
# the terms of the GNU General Public License as published by the Free Software
# Foundation, either version 3 of the License, or (at your option) any later
# version.
#
# This program is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
# FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
# details.
#
# You should have received a copy of the GNU General Public License along with
# this program. If not, see <https://www.gnu.org/licenses/>.

# Lychee must be run against every page as served in order to check
# every link is correct. Lychee does not support recursion so we have to
# feed it the sitemap ourselves.
# [Add recursive option](https://github.com/lycheeverse/lychee/issues/78)

kill_hugo() {
  echo "Killing $hugo_pid"
  kill "$hugo_pid"
}

hugo_port=$(lsof -i4 -P -n | grep hugo | grep LISTEN | cut -d: -f2 | cut -d' ' -f1)

if [[ -z "$hugo_port" ]]; then
  echo -n "No hugo server running, starting our own..."
  nohup hugo server -p 1313 --destination "$BUILDDIR/hugo_server" \
    --bind 127.0.0.1 --disableFastRender --source hugo \
    --cleanDestinationDir >&/dev/null &
  hugo_pid=$!
  hugo_port=1313
  echo "process $hugo_pid running."
  sleep 1
  trap kill_hugo 0 1 2 3 6 14 15
fi

echo "Fetch sitemap..."
sitemap=$(curl -s --retry-all-errors --retry 5 --retry-delay 1 http://localhost:"$hugo_port"/sitemap.xml) || exit 1
# https://superuser.com/a/1005060
read -r -d '' sitemap_xslt <<'EOF'
<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0"
    xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
    xmlns:s="http://www.sitemaps.org/schemas/sitemap/0.9">

  <xsl:output method="text" />

  <xsl:template match="s:url">
    <xsl:value-of select="s:loc" />
    <xsl:text>
</xsl:text>
  </xsl:template>

</xsl:stylesheet>
EOF

links=$(xsltproc <(echo "$sitemap_xslt") <(echo "$sitemap")) || exit 1
# xargs -n1 cleans and trims the input as some sitemap entries have whitespace
echo "$links" | xargs -n1 | sort | tee sitemap.txt | xargs lychee -v
