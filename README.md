<!--
SPDX-FileCopyrightText: 2024 Galagic Limited, et al. <https://galagic.com>

SPDX-License-Identifier: CC-BY-SA-4.0

Source of the website [innercitylearners.com](https://innercitylearners.com)

For full copyright information see the AUTHORS file at the top-level
directory of this distribution or at
[AUTHORS](https://gitlab.com/thegalagic/innercitylearners_com/AUTHORS.md)

This work is licensed under the Creative Commons Attribution 4.0 International
License. You should have received a copy of the license along with this work.
If not, visit http://creativecommons.org/licenses/by/4.0/ or send a letter to
Creative Commons, PO Box 1866, Mountain View, CA 94042, USA.
-->

# Inner City Learners Website

[This repository](https://gitlab.com/thegalagic/learners/innercitylearners_com)
is the source for [https://innercitylearners.com](https://innercitylearners.com).

## Usage

See `CONTRIBUTING.md` for how to get setup for making changes.

To create a new post (from the `hugo` dir):

```bash
hugo new posts/000N-description-like-this/index.md
```

To create a new gallery and add an image post to it:

```
hugo new galleries/YYYYMM-nice-description/_index.md
hugo new --kind galleriespost galleries/YYYYMM-nice-description/image-desc/index.md
```

To change the order of images in a gallery change the weight in the frontmatter.
Screenshots should be taken on mobile and scaled to 540 × 1147 (cropping system
bar) and run through something like `pngcrush`.

When adding new work we are trying to keep to IndieWeb and Digital Garden
principles:

- Digital Gardening
  - Add suitable tags
  - Add backlinks where suitable
  - Add related posts if suitable
  - Consider updating the homepage with link to new content
  - Does it need a table of contents?
  - Does any content need a changelog? Here's a nice example
    [Sympolymathesy, by Chris Krycho](https://v5.chriskrycho.com/essays/jj-init/)
  - Is it part of a series? Add that to the start and have an index page for
    whole series. Example:
    [Jonas Hietala: Browse posts with telescope.nvim](https://www.jonashietala.se/blog/2024/05/08/browse_posts_with_telescopenvim/)
- Avoid JS and dependencies
- IndieWeb: if you syndicate (POSSE) add a link in the content to the
  syndicated conversations.

## To Do

Niceties:

- No webp
- Nice quotes and notes. Favourite posts.
  [Jonas Hietala: Microfeatures in my blog](https://www.jonashietala.se/blog/2024/07/09/microfeatures_in_my_blog/)
- Could our post status look nicer, an icon some colour?
- Search
- Add RSS to source so we can verify changes
- Fix [mdformat breaks Hugo shortcodes by escaping `<`](https://github.com/executablebooks/mdformat/issues/402)
- There's a number of things we fix by hand that could be automatically flagged
  - Images that are too large
  - Images that are no lazy loaded (except for those above fold)
  - Links which are not descriptive
- Ideally we serve small images to small screen sizes
- Do all our pages need to have open graph support for social media sharing?

Longer-term:

- Use ninjasub to gen ninja build
  - Have to package for nix

## Contributing

Please see `CONTRIBUTING.md`

## Thanks and Licensing

Licensing is below. For our thanks see the post in this repo: [hugo/content/posts/0002-thanks-and-licensing/index.md](https://gitlab.com/thegalagic/learners/innercitylearners_com/-/blob/main/hugo/content/posts/0002-thanks-and-licensing/index.md)
which is also online at [thanks and licensing](https://innercitylearners.com/posts/20241003104229/thanks-and-licensing/).

We declare our licensing by following the REUSE specification - copies of
applicable licenses are stored in the LICENSES directory. Here is a summary:

- All source files that are stating a viewpoint are licensed under
  [Creative Commons Attribution-NoDerivatives 4.0 International](https://creativecommons.org/licenses/by-nd/4.0/).
  All images unless otherwise noted are also under this license.
- All source code is licensed under GPL-3.0-or-later.
- All configuration including all text when extracted from code, is licensed
  under CC-BY-SA-4.0.
- Where we use a range of copyright years it is inclusive and shorthand for
  listing each year individually as a copyrightable year in its own right.

For more accurate information, check individual files.
