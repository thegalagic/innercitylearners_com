---
title: '{{ replace .File.ContentBaseName "-" " " | title }}'
date: '{{ .Date }}'
draft: false
weight: 10
---

{{< gallerycontrols >}}

{{< figure src="FILENAME"
width="WIDTH"
height="HEIGHT"
caption="CAPTION"
alt="ALT" >}}
