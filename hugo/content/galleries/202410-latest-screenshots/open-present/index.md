---
title: Open Present
date: '2024-10-09T14:30:42+01:00'
draft: false
weight: 60
---

{{< gallerycontrols >}}

{{< figure src="screen06.png" width="540" height="1147"
      caption="If we have two stars or more we can open the present."
      alt="We are back to the grassy map but now at the bottom we have a button saying 'Open Present'.">}}

Mentioned in post [Screenshots](/posts/20241004133608/screenshots/).
