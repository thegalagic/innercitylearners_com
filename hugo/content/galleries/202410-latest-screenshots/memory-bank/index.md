---
title: Memory Bank
date: '2024-10-09T14:30:42+01:00'
draft: false
weight: 30
---

{{< gallerycontrols >}}

{{< figure src="screen03.png" width="540" height="1147"
caption="Looking for challenges in the Memory Bank."
alt="A modal dialogue is now over the grassy map showing us several multiplication challenges we can select.">}}

Mentioned in post [Screenshots](/posts/20241004133608/screenshots/).
