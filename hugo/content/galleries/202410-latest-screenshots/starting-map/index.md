---
title: Starting Map
date: '2024-10-09T14:30:40+01:00'
draft: false
weight: 20
---

{{< gallerycontrols >}}

{{< figure src="screen02.png" width="540" height="1147"
caption="The starting map, just grass and a present."
alt="We see a grassy map, in the middle sits a small pink present. At the top we have a count of hearts (0) and stars (0). At the bottom there are buttons labelled exit, memory bank and build.">}}

Mentioned in post [Screenshots](/posts/20241004133608/screenshots/).
