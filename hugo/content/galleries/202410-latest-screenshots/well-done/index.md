---
title: Well Done
date: '2024-10-09T14:30:42+01:00'
draft: false
weight: 50
---

{{< gallerycontrols >}}

{{< figure src="screen05.png" width="540" height="1147"
caption="Correct answers win us stars."
alt="The player has filled in correct answers for the five times table and a message at the bottom says 'Well done!'">}}

Mentioned in post [Screenshots](/posts/20241004133608/screenshots/).
