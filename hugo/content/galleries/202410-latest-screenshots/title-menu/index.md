---
title: Title Menu
date: '2024-10-09T14:30:39+01:00'
draft: false
weight: 10
---

{{< gallerycontrols >}}

{{< figure src="screen01.png" width="540" height="1147"
caption="The title menu."
alt="The title screen reads 'Inner City Learners' with two buttons: play and exit." >}}

Mentioned in post [Screenshots](/posts/20241004133608/screenshots/).
