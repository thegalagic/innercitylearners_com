---
title: 'Five Times Table'
date: '2024-10-09T14:30:42+01:00'
draft: false
weight: 40
---

{{< gallerycontrols >}}

{{< figure src="screen04.png" width="540" height="1147"
      caption="Taking on the five times table."
      alt="We see three questions from the five times table with empty boxes to fill in our answers and a submit button." >}}

Mentioned in post [Screenshots](/posts/20241004133608/screenshots/).
