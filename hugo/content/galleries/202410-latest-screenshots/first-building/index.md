---
title: First Building
date: '2024-10-09T14:30:42+01:00'
draft: false
weight: 70
---

{{< gallerycontrols >}}

{{< figure src="screen07.png" width="540" height="1147"
caption="After opening the present our first building arrives."
alt="We now see a new building on the map, just a simple two-storey red house with a small section of road in front.">}}

Mentioned in post [Screenshots](/posts/20241004133608/screenshots/).
