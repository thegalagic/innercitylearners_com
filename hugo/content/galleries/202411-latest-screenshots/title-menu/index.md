---
title: Title Menu
date: '2024-11-07T15:56:41Z'
draft: false
weight: 10
---

{{< gallerycontrols >}}

{{< figure src="title-menu.png" width="540" height="1147"
caption="The title menu."
alt="The title screen reads 'Inner City Learners' with two buttons: play and exit." >}}

Mentioned in post [Screenshots](/posts/20241108141230/screenshots/).
