---
title: Game Start
date: '2024-11-07T15:56:41Z'
draft: false
weight: 20
---

{{< gallerycontrols >}}

{{< figure src="game-start.png" width="540" height="1147"
caption="Start of a new city."
alt="We see an empty field. At the top of the screen a display shows we have 0 hearts and 0 stars. At the bottom are buttons labelled exit, memory bank and build." >}}

Mentioned in post [Screenshots](/posts/20241108141230/screenshots/).
