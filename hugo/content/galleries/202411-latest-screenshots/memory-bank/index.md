---
title: Memory Bank
date: '2024-11-07T15:56:41Z'
draft: false
weight: 30
---

{{< gallerycontrols >}}

{{< figure src="memory-bank.png" width="540" height="1147"
caption="Looking for challenges in the Memory Bank."
alt="A modal dialogue is now over the grassy map showing us several multiplication challenges we can select.">}}

Mentioned in post [Screenshots](/posts/20241108141230/screenshots/).
