---
title: Road extended
date: '2024-11-07T15:56:41Z'
draft: false
weight: 90
---

{{< gallerycontrols >}}

{{< figure src="road-extended.png" width="540" height="1147"
caption="Extending the road either side of the house."
alt="We see a red house in the city with a road in front of it that's been extended on both sides." >}}

Mentioned in post [Screenshots](/posts/20241108141230/screenshots/).
