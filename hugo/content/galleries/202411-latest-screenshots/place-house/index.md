---
title: Place House
date: '2024-11-07T15:56:41Z'
draft: false
weight: 70
---

{{< gallerycontrols >}}

{{< figure src="place-house.png" width="540" height="1147"
caption="Choosing a house to place in our city."
alt="A modal dialog appears over the empty city asking us to choose from one of 3 houses to place in the city." >}}

Mentioned in post [Screenshots](/posts/20241108141230/screenshots/).
