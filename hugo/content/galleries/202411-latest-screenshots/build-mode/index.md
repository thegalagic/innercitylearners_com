---
title: Build Mode
date: '2024-11-07T15:56:41Z'
draft: false
weight: 60
---

{{< gallerycontrols >}}

{{< figure src="build-mode.png" width="540" height="1147"
caption="Build mode activated, one build site available."
alt="In the middle of our empty field a dotted rectangle with a plus in the middle shows where we can build." >}}

Mentioned in post [Screenshots](/posts/20241108141230/screenshots/).
