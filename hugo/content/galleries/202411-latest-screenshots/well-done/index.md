---
title: Well Done
date: '2024-11-07T15:56:41Z'
draft: false
weight: 50
---

{{< gallerycontrols >}}

{{< figure src="five-times-table-complete.png" width="540" height="1147"
caption="Correct answers win us stars."
alt="The player has filled in correct answers for the five times table and a message at the bottom says 'Well done!'">}}

Mentioned in post [Screenshots](/posts/20241108141230/screenshots/).
