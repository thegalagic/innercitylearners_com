---
title: House and Road
date: '2024-11-07T15:56:41Z'
draft: false
weight: 80
---

{{< gallerycontrols >}}

{{< figure src="house-and-road.png" width="540" height="1147"
caption="Our first house is built, with its own road."
alt="A small red house has now been placed in the city. Below it a small section of road which has controls to grow or shrink it." >}}

Mentioned in post [Screenshots](/posts/20241108141230/screenshots/).
