---
title: Five Times Table
date: '2024-11-07T15:56:41Z'
draft: false
weight: 40
---

{{< gallerycontrols >}}

{{< figure src="five-times-table.png" width="540" height="1147"
caption="Taking on the five times table."
alt="We see three questions from the five times table with empty boxes to fill in our answers and a submit button." >}}

Mentioned in post [Screenshots](/posts/20241108141230/screenshots/).
