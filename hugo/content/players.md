---
title: Players
menu: main
weight: 20
date: 2024-06-20T11:32:13+01:00
---

{{< figure src="featured.png" width="720" height="300" alt="A large magnifying glass is held over a pixilated human character with blond hair and blue clothes" >}}

We've got lots planned for players! Here is a taste:

- Create and bring life to your own unique city. Start from an empty field and
  build all the way up to a bustling metropolis!
- As you complete learning challenges extend the city with new buildings,
  decorations and new residents.
- Play mini-games you find around the city - in buildings and on the streets.
- Watch as your residents explore the city and find ways to amuse themselves
  with what you have built. You can even join in with their games!
