---
title: A New Theme
date: '2024-10-03T12:06:17+01:00'
draft: false
aliases: ./
---

Yesterday we changed the theme for our website's design from
[Ananke](https://themes.gohugo.io/themes/gohugo-theme-ananke/) to
[Bear Cub](https://themes.gohugo.io/themes/hugo-bearcub/). The change at this
stage is mostly cosmetic. However we are looking to make this place more of
a 'digital garden' in future and a simple theme like Bear Cub will help. We'll say
more on digital gardens soon too.

{{< aside type="update" date="2024-10-07" msg="We did! Take a look at the post [a digital garden](/posts/20241003134608/a-digital-garden/)." >}}

For now it means the website is smaller and leaner with a cleaner look.
I am also impressed with Bear Cub's support for [accessibility](https://themes.gohugo.io/themes/hugo-bearcub/#accessible)
and multiple languages. It would be wonderful to offer translated versions of this
site in future.

The site is built with [Hugo](https://gohugo.io/). You can see a full list of
the projects we rely on in our new page: [thanks and licensing](/posts/20241003104229/).
The source code for the whole site is available at
[innercitylearners_com (gitlab.com)](https://gitlab.com/thegalagic/innercitylearners_com).

We tooted about this on [Mastodon](https://fosstodon.org/@innercitylearners/113243354896039546).
