---
title: Free and Open Source Games (Series)
date: '2024-10-14T13:57:44+01:00'
draft: false
aliases: ./
status: Emerging thoughts
---

While we're working on our game we thought it would be good to talk about some
of the great free and open source games that have inspired us. In particular
those that might appeal to parents, carers and teachers.

In this series we're going to look at games which offer something for younger
children both in terms of entertainment and education. The full list of this
series is **below**. We're just getting started but more articles will appear
in this list as they're published. To find out how to stay up-to-date see our
[contact](/contact/) page.

- [Burger Party](/games/20241014120700/burger-party/)
