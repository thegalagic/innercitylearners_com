---
title: Introducing Inner City Learners
date: 2024-06-14T14:22:14+01:00
draft: false
aliases: ./
---

{{< figure src="featured.png" width="808" height="560" alt="View of a small city from above with a line of buildings, a park with a pond, some awnings all set amongst grass." >}}

Hello and welcome! Inner City Learners will be an educational city building game
for children in primary education. It will also be a tool for parents and teachers
to let them design learning challenges, give rewards and track progress within
the game. The best part is it will be [free and open source software](https://en.wikipedia.org/wiki/Free_and_open-source_software)
with all the goodness that brings.

We are in the early stage of development and excited to be sharing our progress
in the coming weeks and months as we work towards a first release. Keep an eye
on this website and our social media account as there will be more content
arriving.

The best ways to keep up-to-date are:

- Follow us on the Fediverse [@innercitylearners@fosstodon.org](https://fosstodon.org/@innercitylearners)
- Subscribe to our [RSS feed](/index.xml)

...and you can always say hi at
[hello@innercitylearners.com](mailto:hello@innercitylearners.com)!
