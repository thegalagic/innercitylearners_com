---
title: Try It
date: '2024-10-04T12:16:08+01:00'
draft: false
aliases: ./
---

{{< aside type=warning msg="The game is in a primitive state at the moment!" >}}

Here's what you can do:

- Move around an empty grass map.
- Undertake some simple multiplication challenges.
- Earn stars if successful in the challenges.
- Activate build mode, showing one building site in the city.
- Tapping on the building site you can choose a house to place.
- Once a house is placed a road appears below it. In build mode the road can
be extended or shrunk by tapping on the road controls.
- That's it!

Check out the [screenshots](/posts/20241108141230/screenshots/) too.

We are not yet available in app stores but coming soon. In the meantime you can
try our latest development builds:

- For Linux, Windows and MacOS download the
  [latest desktop JAR](https://gitlab.com/thegalagic/learners/innercitylearners/-/jobs/artifacts/main/raw/desktop/build/libs/innercitylearners.jar?job=build).
  Either double-click the download or use the command line: `java -jar innercitylearners.jar`. Your must have Java 21+ already installed for this to
  work.
- For Android download the
  [latest APK](https://gitlab.com/thegalagic/learners/innercitylearners/-/jobs/artifacts/main/raw/android/build/outputs/apk/release/innercitylearners.apk?job=build).

These instructions should be up-to-date but you can also check the docs in the
code repository in case of problems: [installation](https://gitlab.com/thegalagic/learners/innercitylearners#install).

Any issues? Feedback? We'd love to hear what you're thinking! Here's how to
[reach out](/contact/).
