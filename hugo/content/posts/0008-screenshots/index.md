---
title: Screenshots
date: '2024-11-08T14:12:30Z'
draft: false
aliases: ./
---

Here's our latest screenshots. The concept of opening presents in the city 
is gone. We now have a simple build mode in the game that lets you choose one
of three buildings. Once a building is placed in the city you can extend the
road that appears in front of it.

In future we'll have more places to build, more buildings and roads will have
junctions as well.

Want to play it now? Find out how at [try it](/posts/20241004121608/try-it/).

We tooted about this on [Mastodon](https://fosstodon.org/@innercitylearners/113448295297854421).

{{< gallery "202411-latest-screenshots" >}}
