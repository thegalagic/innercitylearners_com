---
title: Thanks and Licensing
date: '2024-10-03T10:42:29+01:00'
draft: false
aliases: ./
---

This website wouldn't exist without the hard work of the following projects who
have our deep gratitude:

- [Hugo](https://gohugo.io/) and our theme [Bear Cub](https://github.com/clente/hugo-bearcub)
  ᕦʕ •ᴥ•ʔᕤ Read more about our choice at [a new theme](/posts/20241003120617/)
- [Git](https://git-scm.com/), [devenv.sh](https://devenv.sh/), [ninja](https://ninja-build.org/),
  [gitleaks](https://github.com/gitleaks/gitleaks), [lychee](https://github.com/lycheeverse/lychee)
  and [REUSE](https://reuse.software/)
- [GitLab](https://gitlab.com/)
- Many of the graphics throughout the site are derived from the talented creators
  [Kenney](https://kenney.nl/) and [LYASeeK](https://lyaseek.itch.io/)

We don't use any JavaScript.

The site is copyright Galagic Limited 2024. For full license information please see
our source repository: [innercitylearners_com (gitlab.com)](https://gitlab.com/thegalagic/innercitylearners_com).
