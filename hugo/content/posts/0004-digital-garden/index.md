---
title: A Digital Garden
date: '2024-10-03T13:46:08+01:00'
draft: false
aliases: ./
status: Emerging thoughts
---

The wait for perfection never ends. So on this site we're going to
embrace imperfection and make this a [digital garden](https://maggieappleton.com/garden-history).

On some pages you may now see a phrase in the byline (like at the top of this page):

- *Emerging thoughts* for early ideas.
- *Solidifying nicely* for work that has been edited once or more and for which
  we have some confidence.
- *Probably complete* for work that we believe is complete, though it will still
  receive minor corrections and updates.

These statuses will link back to here. They are an indicator for you the reader
of the state of a post. We simple ask that we be allowed to be seen as fallible
and open to corrections. This is part of digital gardening's idea of an
understanding between visitor and gardener.

We hope that in the long term this will make this site more playful and experimental.
Knowing our words can evolve should mean we can publish more freely and reduce
the mental barriers of working in public.
