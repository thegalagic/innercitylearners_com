---
title: Contact
menu: main
weight: 60
date: 2024-10-02T12:59:07+01:00
---

The best ways to keep up-to-date are:

- Follow us on the Fediverse [@innercitylearners@fosstodon.org](https://fosstodon.org/@innercitylearners)
- Subscribe to our [RSS feed](/index.xml)

...and you can always say hi at
[hello@innercitylearners.com](mailto:hello@innercitylearners.com)!
