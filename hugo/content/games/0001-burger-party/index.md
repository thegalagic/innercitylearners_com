---
title: Burger Party
date: '2024-10-14T12:07:00+01:00'
draft: false
aliases: ./
status: Emerging thoughts
params:
  game:
    author: '[Aurélien Gâteau](https://agateau.com/about-me/)'
    version: 1.4.4
    ratings:
      PEGI: 3
    start:
      year: 2013
      sources:
        - https://github.com/agateau/burgerparty/commit/2e7b90a0312d629ea4117ac29dcc2f97faea43bd
        - https://www.indiedb.com/games/burger-party/articles
    license:
      code: GPL 3.0+ and Apache 2.0
      assets: Mix of creative commons licenses and SIL open font license
      source: https://github.com/agateau/burgerparty#license
    available:
      android:
        fdroid: https://f-droid.org/packages/com.agateau.burgerparty/
        play: https://play.google.com/store/apps/details?id=com.agateau.burgerparty
    source: https://github.com/agateau/burgerparty
    education: None
---

*This article is part of our series on [Free and Open Source Games](/posts/20241014135744/free-and-open-source-games-series/).*

[Burger Party](https://agateau.com/projects/burgerparty/) is an Android game
from [Aurélien Gâteau](https://agateau.com/about-me/) that challenges you to
complete burger orders at a restaurant as fast and as accurately as you can.
It's a well crafted game with near-universal appeal. Read on to find out more.

{{< figure src="Pirate_Burgers.jpg" width="720" height="432" caption="Orders come in fast from these hungry Pirates" alt="A queue of pirates stand in front of us, the first one asking for a layered burger and fries. We stand behind a tray ready to compose an order. At the bottom of the screen we can choose from ingredients such as buns, patties, cheese and sauces." >}}

Table of contents:

<!--toc:start-->

- [Overview](#overview)
- [The Experience](#the-experience)
- [Audience](#audience)
- [Educational Value](#educational-value)
- [Content Advice](#content-advice)
- [Supporting the Developer](#supporting-the-developer)
- [Final Words](#final-words)
- [Game Information](#game-information)

<!--toc:end-->

## Overview

Burger Party challenges us to complete burger orders in restaurants around the
world with speed and accuracy. After selecting a difficulty setting a silent
video tutorial shows us how to play and then it's straight into the action.

We see a queue of customers waiting in our restaurant. The front customer asks
for their order which is depicted as an exploded burger in a speech bubble. We
must tap the ingredients of the burger, in order, starting from the bottom and
moving upwards. An arrow helpfully points at the next layer to complete. Any
mistakes mean the order is thrown away and we have to start again. Depending on
the difficulty setting you may be racing the clock but even without that the
customer's faces register dismay the longer they wait.

Each order completed gains you coins. A maximum of three coins are available
for a fully accurate order, down to just one coin if mistakes are made. Three
coins gains you one star and each level has a total of three stars available.

Levels unlock as you progress. Some levels when unlocked have a 'gift': an
additional food item for your kitchen that customers can order, making things
increasingly complex. The new item is presented in a small cut scene. After
every 15 levels you unlock a new restaurant location with three to visit. When
this happens you are treated to another cut scene showing you flying to the new
location. Restaurants vary by their background graphics, the appearance of
the customers and some special food items (coconut drinks in the Caribbean for
example).

The game has many small rewards for play. If you get consecutive orders correct
you'll see messages such as "2x combo". There are several achievements to win such
as "burger god" (serve 200 burgers) and "all stars" (finish all levels with
three stars). A perfect run gets rewarded with a message and the level is
marked with a 'Perfect!' banner.

{{< figure src="Titles.jpg" width="720" height="432" loading="lazy" caption="The Title screen" alt="On the counter is an impossibly big burger with 6 patties and cheese accompanied by fries and an ice cream." >}}

{{< figure src="Location_Selector.jpg" width="720" height="432" loading="lazy" caption="Select a Location: New York, Caribbean, Japan or Practise Area" alt="Four big vertical buttons fill a yellow screen each one a different location we can visit." >}}

{{< figure src="Level_Selector.jpg" width="720" height="432" loading="lazy" caption="Select a Level" alt="15 blue buttons in three rows of five are shown on a yellow background, each representing a different level. They are labelled foe example '2-5'" >}}

## The Experience

The choice of a food theme is smart due to its universal appeal. The developer
has crafted a challenging game with features you'd expect from a professional
studio such as the level selector, animated title screens, fun cut scenes,
careful sound design and a settings menu (though you can only toggle the music
on and off). This attention to detail elevates the game above many others.

There's a strong sense of progression throughout play whether its by completing
levels, travelling to new locations, gaining new ingredients, completing combos
or unlocking achievements. All of these moment are accompanied by
notifications or animations which, while simple, add a lot of character and joy
to the game.

The graphics are bright and bouncy. Small touches like a swinging animation on
the presents dangling from the level buttons speak again to the quality of the
game. There is a jolly soundtrack featuring steel drums and sound effects
accompany the game's actions. Each layer of the burger sounds different as it
drops onto the plate and there are children's voices to celebrate the end of a
level or a perfect score.

There's a real satisfaction in the mastery of this game. The food items in your
kitchen do not move around between levels so it's possible to learn to 'touch
type' orders once proficient.

Complete any four levels and you'll unlock the practice area. It's an empty
restaurant allowing you the time to create any burger you like. In fact the
game even allows you to create giant towering burgers of huge height, which can
be a delight for young players to discover.

{{< figure src="Practice_Area.jpg" width="720" height="432" loading="lazy" caption="Building big burgers in the Practise Area" alt="On the counter is an impossibly big burger with 6 patties and cheese accompanied by fries and an ice cream." >}}

## Audience

In Google's European Play Store the game has a PEGI 3 rating: 'suitable for
all age groups'. There are three difficultly levels: kids, normal and expert.
The kids mode is perfect for younger children who may struggle with the time
limits or anyone who prefers to play without time pressure. This is a great
addition. However note that customers can still get upset (a change in facial
expression) if they wait too long.

It's a great pocket game that you can just pick up and play any time. We'd
recommend it to stave off boredom during travel or any small gaps in the day.

## Educational Value

This is a game of recognising and copying patterns, memorising the position of
ingredients in the kitchen and the use of fine motor skills. The optional
time pressure adds a way to increase difficulty once the basics are mastered.
As such it would works well for young children of all ages.

Whilst the game is not intended as a teaching tool it could be used to support
learning food vocabulary and colours. In addition the player is exposed to
small numbers via the level selector (up to 15) and counting through the coins
and stars system (up to three).

## Content Advice

It should be clear from the title this is a game which depicts animal products
and their (implied) consumption. Some locations and their customers are
depicted as cartoonish regional stereotypes such as pirates, ninjas, geishas
etc.

There are no trackers or advertising in the game and no network access is
required.

## Supporting the Developer

The developer lists several ways you can [support](https://agateau.com/support/)
their work by donation and they also offer merchandise on [Redbubble](https://www.redbubble.com/people/agateau/shop).

## Final Words

This game is well worth downloading. Though it's not designed to be educational
it provides great entertainment, is perfectly suited for children and there is
much for them to enjoy in this slick production.

We tooted about this article on [Mastodon](https://fosstodon.org/@innercitylearners/113329093504369137).

## Game Information

{{< gamedetails >}}
