---
title: Parents & Carers
menu: main
weight: 30
date: 2024-06-20T11:32:13+01:00
---

{{< figure src="featured.png" width="720" height="300" alt="Two rows of five different pixelated characters on a plain light brown background" >}}

As a parent and carer we understand how busy life can be. This game will take
the stress out of helping your children reach their potential:

- Keep track of their progress - get notified when your child completes learning
  challenges you care about.
- Set your own learning challenges. Do they need a little extra maths practise?
  Add a challenge of your own or copy and edit a pre-existing one in seconds.
- Automatically reward your child with in-game items when they reach the milestones
  you care about. Thrill them with a new building for their city!
- Stay in sync with school by pairing your child's game with a participating school
  or teacher. They will be able to track, set and reward learning challenges
  to complement their in-class learning.
