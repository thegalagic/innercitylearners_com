---
title: Home
date: 2024-06-20T11:32:13+01:00
menu: main
weight: 10
---

{{< figure src="featured.png" width="720" height="300" alt="View of a small city from above with a line of buildings all set amongst grass." >}}

Welcome! We are creating an educational city building game for
children aged 6-12 years. It's still in early development but if you're
feeling brave you are welcome to [try it](/posts/20241004121608/try-it/) or
check out our [screenshots](/posts/20241108141230/screenshots/).

May we suggest reading our [introduction
post](/posts/0001-introducing/) to find out more about the game
or dive straight into how it benefits [players](/players), [parents &
carers](/parentsandcarers) and [teachers](/teachers).

______________________________________________________________________

## What's New

### November 2024

New [screenshots](/posts/20241108141230/screenshots/) have been uploaded! We
now have a simple build mode in the game that lets you choose one of three
buildings. Once a building is placed in the city you can extend the road that
appears in front of it. We'll be evolving this build mode to show how many
stars your plan costs and give the option to cancel building.

### October 2024

We've started a new article series on [Free and Open Source
Games](/posts/20241014135744/free-and-open-source-games-series/) that would
appeal to parents, carers and teachers. Check out the first entry on the
wonderful [Burger Party](/games/20241014120700/burger-party/).

Do you like our new look? Read more about the change at
[a new theme](/posts/20241003120617/a-new-theme/) and why we're turning this
place into a [digital garden](/posts/20241003134608/a-digital-garden/).
