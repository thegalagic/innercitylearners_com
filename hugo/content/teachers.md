---
title: Teachers
menu: main
weight: 40
date: 2024-06-20T11:32:13+01:00
---

{{< figure src="featured.png" width="720" height="300" alt="A pixelated character stands by a blackboard" >}}

There's not enough time for another complicated teaching tool so we've kept
Inner City Learners simple:

- Setup your students or class in seconds by import or pasting in names. Organise
  and group them however you need.
- Connect to your students by issuing invite links they can scan from inside the
  game.
- Set learning goals based on the national curriculum or create your own.
  Re-use our extensive collection of challenges or customise them to the
  exact needs of your students.
- Receive notifications when goals are achieved and customise reports to help you
  assess students at significant milestones.
- Reward your students with in-game items tailored to classroom themes and their
  interests.
