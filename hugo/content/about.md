---
title: About
menu: main
weight: 70
date: 2024-10-03T11:28:13+01:00
---

We are an independent developer and parent working on this new city building
game aimed at children in primary education aged 6-12 years. Read more about
the game on our [introduction post](/posts/20240614142214/).

We're active on Mastodon as
[@innercitylearners@fosstodon.org](https://fosstodon.org/@innercitylearners)
where we also [introduced ourselves](https://fosstodon.org/@innercitylearners/112671858858109693):

{{< figure src="featured.png" width="720" height="467" alt="A social media post with similar text to this page introduction us and the game. There's a link to this site." >}}

We formed a company for our independent work, [Galagic Limited](https://galagic.com/),
which also has its own website.
